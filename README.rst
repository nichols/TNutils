TNutils
=======

.. image:: https://img.shields.io/pypi/v/TNutils.svg
    :target: https://pypi.python.org/pypi/TNutils
    :alt: Latest PyPI version

.. image:: https://travis-ci.org/borntyping/cookiecutter-pypackage-minimal.png
   :target: https://travis-ci.org/borntyping/cookiecutter-pypackage-minimal
   :alt: Latest Travis CI build status

My first python package, random utilities

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`TNutils` was written by `TE Nichols <thomas.nichols@bdi.ox.ac.uk>`_.
