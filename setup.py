import setuptools

with open('requirements.txt', 'rt') as f:
    install_requires = [l.strip() for l in f.readlines()]

setuptools.setup(
    name="TNutils",
    version="0.1.0",
    url="https://git.fmrib.ox.ac.uk/nichols/TNutils",

    author="TE Nichols",
    author_email="thomas.nichols@bdi.ox.ac.uk",

    description="My first python package, random utilities",
    long_description=open('README.rst').read(),

    entry_points = {
        'console_scripts' : [ 'immax =  TNutils.imstats:main_immax',
                              'immean = TNutils.imstats:main_immean',
        ]
        },

    packages=setuptools.find_packages(),

    install_requires=install_requires,

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
)
