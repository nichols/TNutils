#!/usr/bin/env python3

import sys
import numpy as np
import nibabel as nib

def immax(Fns):
    """Find max of some images."""

    if isinstance(Fns, str):
        Fns=[Fns]

    Max=[]
    for i in range(len(Fns)):
        fn = Fns[i]
        try:
            # np.asarray() not required for newer versions of nibabel
            #                    vvvvvvvvvv
            Max.append(np.nanmax(np.asarray((nib.load(fn).get_data()))))
        except Exception as e:
            print("ERROR: "+str(e))
            exit()

    return(Max)
    

def immean(Fns):
    """Find mean of some images."""

    if isinstance(Fns, str):
        Fns=[Fns]

    Mean=[]
    for i in range(len(Fns)):
        fn = Fns[i]
        try:
            # np.asarray() not required for newer versions of nibabel
            #                      vvvvvvvvvv
            Mean.append(np.nanmean(np.asarray((nib.load(fn).get_data()))))
        except Exception as e:
            print("ERROR: "+str(e))
            exit()
            
    return(Mean)
    

def PrintResults(Fns,Vals):
    print('\n'.join(['{:40s} : {:08f}'.format(nm,mx) for (nm,mx) in zip(Fns,Vals)]))


def ScriptUtil(Usage):

    if len(sys.argv)==1:
        print(Usage)
        sys.exit(1)
    else:
        Imgs = sys.argv[1:]

    return(Imgs)

def main_immax():
    """Script wrapper for immax."""
    Usage="""
Usage: immax.py img1 [img2 ...]

Print the maximum of one or more images. 
_________________________________________________________________________
T. Nichols
"""
    Imgs=ScriptUtil(Usage)
    mx=immax(Imgs)
    PrintResults(Imgs,mx)

def main_immean():
    """Script wrapper for immean."""
    Usage="""
Usage: immean.py img1 [img2 ...]

Print the mean of one or more images. 
_________________________________________________________________________
T. Nichols
"""
    Imgs=ScriptUtil(Usage)
    mn=immean(Imgs)
    PrintResults(Imgs,mn)

